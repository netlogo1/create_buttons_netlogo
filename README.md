# Intro_to_drawings



If you download the nlogo file, you should be able to open it on your computer (if you have NetLogo installed). It will show you 5 buttons on the interface (random-square, clear, polygon, sep-patch, and random-square-opt). You can access the code in the code window on NetLogo application. If it doesn't work you have the code in the rtf file provided, but you will have to copy and paste it in NetLogo and create the buttons yourself. When you click on a button you will have a drawing that will appear, and you can clear it with the clear button or directly click on another button to draw something else.

Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021
